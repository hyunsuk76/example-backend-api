# 백엔드 API 개발 예제(example-backend-api)
  * backend 개발을 위한 기본 개발환경을 제공한다.  


# 이력
  * v0.0.1-SNAPSHOT
    * 2018.10.17 최초 등록


# 구성
  * 버전
    * example-backend-api-0.0.1-SNAPSHOT
  * 환경
    * windows 10, mac
    * intellij 2018.2.5
    * spring tool suite 4.0.0.RELEASE(build 201809220817)
    * d2coding-1.3.2
    * sourcetree 2.6.10
    * apache-maven-3.5.4
    * h2-1.4.197 (scope:runtime)
    * openjdk 1.8.0.171
    * rabbit mq
    * redis
  * 프레임워크
    * spring boot 2.0.7.RELEASE
      * spring framework 5.0.9.RELEASE
      * spring data jpa
      * spring data redis
      * spring amqp
      * spring websocket
      * spring test(scope:test)
      * spring dev-tools(scope:runtime)
      * embedded tomcat 8.5.34(scope:provided)
      * hibernate 5.2.17.Final
      * hibernate validator 6.0.12.Final
      * hikaricp 2.7.9
      * jackson 2.9.6
    * flyway 5.0.7
    * querydsl 4.1.4
    * lombok 1.18.6
    * commons-lang3 3.7
    * commons-pool2 2.5.0
    * commons-io 2.5
    * lettuce-5.0.5.RELEASE
    * mybatis 1.3.2
    * swagger 2.7.0
    * mapstruct 1.3.0.Final
    * log4jdbc 1.16(profiles:default)
    * jackson-dataformat-yaml 2.9.7
    * postgresql 42.1.4
>>>
  더 자세한 내용은 /{local}/repository/org/springframework/boot/spring-boot-dependencies/2.0.7.RELEASE/spring-boot-dependencies-2.0.7.RELEASE.pom dependency
>>>
    

# 실행
  ## openjdk 8.0.202.08 설치
  * https://github.com/ojdkbuild/ojdkbuild -다운로드 후 설치
  * 설치 확인
    ``` 
      java -version 
    ```  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-jdk-4.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-jdk-3.png" width="400" height="240" />   
        
  ## sourcetree 2.6.10 설치
  * https://www.sourcetreeapp.com 다운로드 후 설치  
  * 설치 안되어있을경우 git 다운로드  
  * repository clone  
    ```
      https://gitlab.com/niceday.lucas/example-backend-api.git
    ```
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sourcetree-1.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sourcetree-2.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sourcetree-3.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sourcetree-4.png" width="400" height="240" />      

  ## spring tool suite 4.0.0.RELEASE 설치
  * https://spring.io/tools 다운로드 후 압축 해제   
  * {sts설치디렉토리}/SpringToolSuite4.ini 수정  
  * sts 실행
  * import-mavent-existing maven projects 실행 후 clone 받은 프로젝트 연결  
  * 프로젝트 - debug as - maven update
  * maven build 완료되면 lombok 설치  
  * 프로젝트 - debug as - maven build(goal : clean package)  
  * 프로젝트 - debug as - maven update  
  * 프로젝트 - debug as - junit test  
  * 모든 테스트 케이스가 success 되어야 정상 동작
  * 현재 테스트 케이스 총 20개(현재 redis 관련 4개 케이스 ignore)
  * RedisTest의 모든 테스트 케이스는 ignore 되어 있으며 redis 설치 후 테스트 가능
    * windows용 redis 다운로드(https://github.com/MicrosoftArchive/redis/releases)
    ``` 
      -vm  
      C:\Program Files\ojdkbuild\java-1.8.0-openjdk-1.8.0.171-1\bin\javaw.exe
    ```    
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-1.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-2.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-3.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-4.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-5.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-6.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-7.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-8.png" width="400" height="240" />   
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-9.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-sts-10.png" width="400" height="240" />  


  ## intellij 2018.2.5 설치
  * https://www.jetbrains.com/idea/download/#section=windows 다운로드 후 설치
  * intellij 실행
  * import project - sourcetree 에서 받는 프로젝트 경로 - maven
  * import-mavent-existing maven projects 실행 후 clone 받은 프로젝트 연결  
  * setting - plugins - browse repositories - lombok install 후 재시작
  * maven build 완료되면 lombok 설치  
  * 프로젝트 - maven - generate sources and update folders
  * 프로젝트 - debug - all tests
  * 모든 테스트 케이스가 success 되어야 정상 동작
  * 현재 테스트 케이스 총 20개(현재 redis 관련 4개 케이스 ignore)
  * RedisTest의 모든 테스트 케이스는 ignore 되어 있으며 redis 설치 후 테스트 가능
    * windows용 redis 다운로드(https://github.com/MicrosoftArchive/redis/releases)
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-1.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-2.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-3.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-4.png" width="400" height="240" /> 
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-5.png" width="400" height="240" />  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/install-intellij-6.png" width="400" height="240" /> 


  ## 기타 실행
  * h2
  * swagger  
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/tool-h2-1.png" width="400" height="240" />
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/tool-h2-2.png" width="400" height="240" />
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/tool-swagger-1.png" width="400" height="240" />
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/tool-swagger-2.png" width="400" height="240" /> 


# 개발
  ## 개발 프로세스
  * 이슈 단계
  issues - boards(open, todo, doing, merged develop, merged master, close)
    * 'open' 단계에 앞으로 진행 될 이슈 생성(마일스톤, 작업자, 일정, 내용)  
    * 스프린트 단위로 한 주가 시작되기 전 'open' -> 'todo' 상태 변경하여 해당 주의 스프린트가 시작 됨을 알림
    * 해당 작업자는 이슈 상태 'doing' 변경하여 이슈 해결 시작을 알림
    * /develop 브랜치를 기반으로 /feature/이슈번호 브랜치 생성하여 개발 시작
    * 개발 완료 후 /feature/이슈번호에 commit/push 후 merge request 생성
    * /feature/이슈번호 맞는 merge request 를 해당 팀원은 모두 코드를 확인하고 변경사항이 있다면 소스 위치에 수정내용 작성, 없다면 '확인 완료' 커맨트 작성
    * 작업자는 merge request 의 모든 커맨트를 확인하고 변경 작업 후에 완료했다는 커맨트를 등록, 'mark as resolved' 선택, 
      만약 '확인 완료' 만 있다면 'mark as resolved' 선택하고 병합
    * 변경요청 한 팀원은 변경 된 내용을 확하고 작업자의 커맨트에 'mark as resolved'
    * 모든 작업이 완료되었다면 /develop 브랜치로 병합
    * 해당 테스크 상태 'merged develop' 변경
    * 위 내용은 스프린트(단위:1주) 단위로 이루어 지며 모든 이슈에 대해서 위와 같이 동일하게 반복
  
  ## 배포 프로세스
  * 개발 : git(branch-develop) - jenkins - deploy - was restart
  * 운영 : git(branch-master)  - jenkins - deploy - was restart
  
  ## 소스 버전 관리 
  * 해당 브랜치 구조는 git-flow 정책을 따름
  
  * 브랜치 구조  
  feature - develop - release - tag - master - hotfix
    * feature : 테스크 단위 브랜치
    * develop : 전체 개발 단위 브랜치
    * release : 릴리즈 단위 브랜치
    * master  : 전체 운영 단위 브랜치
    * hotfix  : 전체운영 핫픽스 단위 브랜치
  * 태그
    * master에 배포되는 모든 단위 기록

  ## 디렉토리 구조
  * 기본 패키지 구성은 MAVEN PACKAGE 기준을 따른다.
  * /src/main/java     : 공통, 비지니스 로직를 모두 포함하는 영역
  * /src/main/resource : 외부 설정 영역
  * /src/test/java     : 테스트 영역(테스트케이스는 /src/main/java/com.lu... 의 테스트 대상 소스의 패키지를 따라간다.)
    * ex. 대상 : com.lu.sample.controller.PostController, 테스트케이스 : com.lu.sample.PostTest, com.lu.sample.PostHelper
  ## 패키지 구조   
  | 패키지 | 설명 |
  |  --------                           |  --------                                               |
  |  com.lu.common                   | 공통 패키지(비니지스 로직이 포함되지 않는 상위 영역 ex. aop, config..) |
  |  com.lu.sample                   | 샘플 패키지(기본 CRUD)                                      |
  |  com.lu.프로젝트                 | 비지니스 로직 영역                                  |
  |  com.lu.프로젝트.common          | 비지니스 공통 영역                                  |
  
  ## 기능별 이하 패키지 구조
  | 패키지 | 설명 |
  |  --------                           |  --------                            |
  | com.lu.sample.domain     | 도메인 영역(Dto, Entity)                         |
  | com.lu.sample.mapper     | 매퍼 영역(Mapper)                                |
  | com.lu.sample.controller | 컨트롤러 영역(Controller)                        |
  | com.lu.sample.service    | 서비스 영역(Service)                             |
  | com.lu.sample.enumerate  | 코드(열거형) 영역 |
  | com.lu.sample.repository | 레파지토리 영역(Repository, Querydsl, Predicate) |

  
  ## 기능별 이하 테스트 패키지 구조
  | 패키지 | 설명 |
  |  --------                           |  --------                            |
  | com.lu.sample   | 테스트 케이스 영역(Test, Helper) |

  ## REST API 디자인
  | 기능         | 함수명     | API                                               | 
  |  --------    |  --------  | --------                                          |
  | 페이지 목록  | getPage    | GET    /api/samples/posts/pages                   | 
  | 목록         | getAll     | GET    /api/samples/posts                         | 
  | 조건 목록    | getAll     | GET    /api/samples/posts/likes/title/content     | 
  | 조건 조회    | get        | GET    /api/samples/posts/like/title/content      | 
  | 관계 목록    | getAll     | GET    /api/samples/posts/{포스트아이디}/comments |
  | 등록         | add        | POST   /api/samples/posts                         |
  | 수정         | modify     | PUT    /api/samples/posts/{포스트아이디}          | 
  | 삭제         | remove     | DELETE /api/samples/posts/{포스트아이디}          | 
  | 존재 여부     | isExists   | HEAD    /api/samples/posts/{포스트아이디}           |
  
  * 페이지 : 마지막 uri에 pages작성
  * 목록 : 복수로 작성
  * 조건 목록 : 복수 뒤에 likes(목록) 를 붙이고 이하로 조건들을 작성
  * 조건 조회 : 복수 뒤에 like(단일) 를 붙이고 이하로 조건들을 작성
  * 관계 목록 : 포스트의 댓글목록을 가져오는 것이다. 이 처럼 도메인의 구조에 맞춰 데이터를 가져올 경우 이와 같이 상하 구조대로 api를 디자인 한다.
  * 등록 : 복수로 작성
  * 수정 : 복수 뒤에 아이디
  * 삭제 : 복수 뒤에 아이디 
  * 존재 여부 : 복수 뒤에 아이디 
  



# 예제
  * 표준 API 개발 시 작성해야 할 내용들을 activity diagram 으로 표현
  * 상세 내용은 아래의 내용을 참조
    <img src="https://gitlab.com/niceday.lucas/images/raw/master/example-backend-api/process-1.png" width="1258" height="488" />  
  ## form
  * form - request/response - api - parameter 기준으로 요청/응답 정보 작성
  * parameter 별 validation 처리가 가능하며 복잡한 validation 의 경우 @AssertTrue, @AssertFalse로 작성
  * javax.validation, org.hibernate.validator 사용
```java
/**   
 * @since       2018.10.02
 * @author      lucas
 * @description post form
 **********************************************************************************************************************/
public class PostForm {

    public static class Request {

        @Getter
        @Setter
        @Builder
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Find {
            
            @ApiModelProperty(value="사용자아이디", required=true)
            @Size(min=5)
            private String userId;
            
            @ApiModelProperty(value="제목", required=true)
            @Size(min=5)
            private String title;
            
            @ApiModelProperty(value="시작일시(형식:yyyy-MM-dd)")
            @PastOrPresent
            @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
            private LocalDate startCreatedAt;
            
            @ApiModelProperty(value="종료일시(형식:yyyy-MM-dd)")
            @PastOrPresent
            @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
            private LocalDate endCreatedAt;
            
            @AssertTrue
            public boolean isValidDateRange() {
                return DateValidator.isValidDateRange(startCreatedAt, endCreatedAt);
            }
        }

        @Getter
        @Setter
        @Builder
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Add {

            @ApiModelProperty(value="사용자아이디", required=true)
            @NotBlank
            @Size(min=5)
            private String userId;
            
            @ApiModelProperty(value="제목", required=true)
            @NotBlank
            @Size(min=5)
            private String title;
            
            @ApiModelProperty(value="내용", required=true)
            @NotBlank
            @Size(min=5)
            private String content;
            
            public Post toPost() {
                return mapper.toPost(this);
            }
        }

        @Getter
        @Setter
        @Builder
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Modify {

            @ApiModelProperty(value="사용자아이디", required=true)
            @NotBlank
            @Size(min=5)
            private String userId;
            
            @ApiModelProperty(value="제목", required=true)
            @NotBlank
            @Size(min=5)
            private String title;
            
            @ApiModelProperty(value="내용", required=true)
            @NotBlank
            @Size(min=5)
            private String content;
            
            public Post toPost() {
                return mapper.toPost(this);
            }
        }
    }
    
    public static class Response {
        
        @Data
        public static class FindAll {

            @ApiModelProperty(value="포스트일련번호")
            private Long   postNo;  
            
            @ApiModelProperty(value="사용자아이디")
            private String userId;     
            
            @ApiModelProperty(value="제목")
            private String title;   
            
            @ApiModelProperty(value="등록일시")
            private String createdAt;
            
            @ApiModelProperty(value="커맨트목록")
            private List<Comment> comments;    
            
            @Data
            public static class Comment {
    
                @ApiModelProperty(value="커맨트일련번호")
                private Long   commentNo;
                
                @ApiModelProperty(value="내용")
                private String content;	
            }
        }
        
        @Data
        public static class FindOne {

            @ApiModelProperty(value="포스트일련번호")
            private Long   postNo;  
            
            @ApiModelProperty(value="사용자아이디")
            private String userId;     
            
            @ApiModelProperty(value="제목")
            private String title;   
            
            @ApiModelProperty(value="등록일시")
            private String createdAt;
            
            @ApiModelProperty(value="커맨트목록")
            private List<Comment> comments;    
            
            @Data
            public static class Comment {
    
                @ApiModelProperty(value="커맨트일련번호")
                private Long   commentNo;
                
                @ApiModelProperty(value="내용")
                private String content;	
            }
        }
    }
    
    
    public static PostMapper mapper = Mappers.getMapper(PostMapper.class);
}        
```
  
  ## mapper
  * request/response 사이에서 dtd - entity 매핑
  * mapstruct 사용(reflection 방식이 아닌 build 시 java code 를 생성하도록하는 개념)
```java
/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post mapper  
 **********************************************************************************************************************/
@Mapper
public interface PostMapper {
    
    Post          toPost   (Add        form);
    Post          toPost   (Modify     form);
    FindAll       toFindAll(Post       entity);
    FindOne       toFindOne(Post       entity);
    List<FindAll> toFindAll(List<Post> entities);

    @Mappings({
         @Mapping(target="postNo",    ignore=true)
    	,@Mapping(target="createdAt", ignore=true)
    })
    Post modify(Post source, @MappingTarget Post target);


    PostMapper mapper = Mappers.getMapper(PostMapper.class);
}

```


  ## controller
  * property.api.end-point : /api
  * restful 기준에 맞는 url 작성
  * @Valid validation 체크 여부
  * pageable 페이징 여부(/api/samples/posts?page=0&size=10&sort=userId,asc&sort=title,desc)
  * service 요청 전달 시 form를 entity 로 매핑, api 응답 반환 시 entity를 form 으로 매핑
    * 즉 request - controller - (form->entity) - service - (entity->form) - controller - response
  * 검색조긴이 있을 경우 predicate, 없을 경우 entity 를 service 로 전달
  * @RequiredArgsConstructor 통한 constructor injection(injection을 위하여 field는 final로 정의)
```java
/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post controller
 **********************************************************************************************************************/
@Api(description="샘플")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class PostController {

	@ApiOperation("포스트 페이징 목록 - 일반적인")
	@GetMapping("/samples/posts")
	public Page<FindAll> getAll(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAll(pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 페이징 목록 - Entity Graph")
	@GetMapping("/samples/posts/entity-graph")
	public Page<FindAll> getAllByEntityGraph(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAllByEntityGraph(pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 페이징 목록 - Querydsl")
	@GetMapping("/samples/posts/query-dsl")
	public Page<FindAll> getAllByQueryDsl(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAllByQueryDsl(PostPredicate.search(find), pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 목록 - Predicate")
	@GetMapping("/samples/posts/predicate")
	public List<FindAll> getAllByPredicate(@Valid Find find){
		return mapper.toFindAll(postService.getAllByPredicate(PostPredicate.search(find)));
	}

	@ApiOperation("포스트 상세 조회")
	@GetMapping("/samples/posts/{postNo}")
	public FindOne get(@PathVariable Long postNo){
		return mapper.toFindOne(postService.get(postNo));
	}

	@ApiOperation("포스트 등록")
	@PostMapping("/samples/posts")
	public FindOne add(@Valid @RequestBody Add add){
		return mapper.toFindOne(postService.add(add.toPost()));
	}

	@ApiOperation("포스트 수정")
	@PutMapping("/samples/posts/{postNo}")
	public FindOne modify(@PathVariable Long postNo, @Valid @RequestBody Modify modify){
		return mapper.toFindOne(postService.modify(postNo, modify.toPost()));
	}

	@ApiOperation("포스트 삭제")
	@DeleteMapping("/samples/posts/{postNo}")
	public void remove(@PathVariable Long postNo){
		postService.remove(postNo);
	}

	@ApiOperation("포스트 존재 여부")
	@RequestMapping(method= RequestMethod.HEAD, value="/samples/posts/{postNo}")
	public void isExists(@PathVariable Long postNo){
		Optional.ofNullable(postService.getOneByPostNo(postNo)).orElseThrow(NotAcceptableException::new);
	}


	private final PostService postService; 
}
```


  ## entity
  * lombok 이용(getter/setter, default constructor, all args constructor, builder 등)
  * base entity 상속으로 등록 시 createdAt, 수정 시 updatedAt 이 자동 설정
  * jpa 기반의 entity 구성
  * OneToMany, ManyToOne 등의 annotation 의 엔티티 관계 정의(현 샘플은 post(1) : (n)comment )
  * default fetch 전략은 lazy 정의
  * n+1 문제는 fetch join(graph entity, querydsl), batch size, subquery 등으로 해결(graph entity, querydsl 권장)
>>>
  더 자세한 내용은 https://docs.spring.io/spring-data/jpa/docs/2.0.7.RELEASE/reference/html
>>>
```java
/**
 * @since       2018.10.02
 * @author      lucas
 * @description post
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity(name="sample_post")
public class Post extends BaseEntity {  
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long   postNo;
    private String userId;
    private String title; 
    private String content;    
    
    
    @OneToMany(mappedBy="post", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private List<Comment> comments;
}

```


  ## service
  * transaction 이 전체 선언되어 있고 조회 시 read olny 설정
  * entity 수정 시에는 mapper 이용
```java
/**
 * @since       2018.10.03
 * @author      lucas
 * @description post service
 **********************************************************************************************************************/
@Service
@RequiredArgsConstructor
public class PostService {

	@Transactional(readOnly=true)
	public Page<Post> getAll(Pageable pageable) {
		return postRepository.findAll(pageable);
	}

	@Transactional(readOnly=true)
	public Page<Post> getAllByEntityGraph(Pageable pageable) {
		return postRepository.findAll(pageable);
	}

	@Transactional(readOnly=true)
	public Page<Post> getAllByQueryDsl(Predicate predicate, Pageable pageable) {
		return postRepository.findAllByQueryDsl(predicate, pageable);
	}

	@Transactional(readOnly=true)
	public List<Post> getAllByPredicate(Predicate predicate) {
		return postRepository.findAll(predicate);
	}

	@Transactional(readOnly=true)
	public Post get(Long postNo) {
		return postRepository.getOne(postNo);
	}

	@Transactional(readOnly=true)
	public Post getOneByPostNo(Long postNo) {
		return postRepository.findOneByPostNo(postNo);
	}

	@Transactional(readOnly=true)
	public Post get(String userId) {
		return postRepository.findByUserId(userId);
	}

	public Post add(Post post) {
		return postRepository.save(post);
	}

	public Post modify(Long postNo, Post post) {
		return mapper.modify(post, postRepository.getOne(postNo));
	}

	public void remove(Long postNo) {
		postRepository.delete(postRepository.getOne(postNo));
	}
	
	@Autowired
	private PostRepository postRepository;
}
```


  ## repository
  * predicate 사용 시 QuerydslPredicateExecutor 선언
  * qyerydsl 을 통한 jpql 사용 시 PostRepositoryQuerydsl 선언, PostRepositoryImpl 구현
  * jpa repository 기본 기능 사용(findAll, saveAll, deleteAllInBatch, getOne, findOne, save, count, exists 등등..)
  * 일반적인 조건은 query creation, predicate
  * 복잡한 조건은 querydsl + predicate
  * jqpl로 작성하기 힘들경우에 한해 mybatis 사용(권장하지 않음)
    * com.lu.sample.api.repository.PostMybatisRepository 참조
>>>
  더 자세한 내용은 https://docs.spring.io/spring-data/jpa/docs/2.0.7.RELEASE/reference/html
>>>
```java
/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post repository 
 **********************************************************************************************************************/
@Repository
public interface PostRepository extends JpaRepository<Post, Long>, QuerydslPredicateExecutor<Post>, PostRepositoryQuerydsl {

	Post findOneByPostNo(Long postNo);
	
	@EntityGraph(attributePaths="comments", type=EntityGraphType.FETCH)
	Page<Post> findAll(Pageable pageable);
	
	@EntityGraph(attributePaths="comments", type=EntityGraphType.FETCH)
	List<Post> findAll(Predicate predicate);
}
```


  ## predicate
  * 검색 조건 명시
  * QPost 는 build 시 /target/generated-sources/java/... 에 자동 생성되어 사용가능
    * 생성이 안되었을 경우 프로젝트 maven build, maven update 후 다시 확인
>>>
  더 자세한 내용은 http://www.querydsl.com/static/querydsl/4.1.4/reference/html
>>>    
    
```java
/**    
 * @since       2018.10.03
 * @author      lucas
 * @description post predicate 
 **********************************************************************************************************************/
public class PostPredicate {
    
    public static Predicate search(Find find) {
        
        QPost          post    = QPost.post;
        BooleanBuilder builder = new BooleanBuilder();
        
        Optional.ofNullable(find.getUserId()).ifPresent(p -> builder.and(post.userId.eq(p)));
        Optional.ofNullable(find.getTitle ()).ifPresent(p -> builder.and(post.title.eq(p)));
        
        return builder;
    }
}
```

  ## querydsl
  * 객체 기반 쿼리 작성
  * QPost 는 build 시 /target/generated-sources/java/... 에 자동 생성되어 사용가능
    * 생성이 안되었을 경우 프로젝트 maven build, maven update 후 다시 확인
>>>
  더 자세한 내용은 http://www.querydsl.com/static/querydsl/4.1.4/reference/html
>>>    
    
```java
/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post repository querydsl
 **********************************************************************************************************************/
public interface PostRepositoryQuerydsl {

    Page<Post> findAllByQueryDsl(Predicate predicate, Pageable pageable);
}

/**    
 * @since       2018.10.03
 * @author      lucas
 * @description post repository impl 
 **********************************************************************************************************************/
public class PostRepositoryImpl extends QuerydslRepositorySupport implements PostRepositoryQuerydsl {

    public Page<Post> findAllByQueryDsl(Predicate predicate, Pageable pageable){
        JPQLQuery<Post> query =  from    (post)
                                .distinct()
                                .leftJoin(post.comments, comment).fetchJoin()
                                .where   (predicate)
                                .orderBy (post.postNo.desc(), comment.commentNo.desc());
        
        return new PageImpl<>(getQuerydsl().applyPagination(pageable, query).fetch(), pageable, query.fetchCount());
    } 
    
    public PostRepositoryImpl() {
        super(Post.class);
    }
    
    private QPost    post    = QPost.post;
    private QComment comment = QComment.comment;
}
```


  ## test helper
  * api 요청/응답을 별도로 작성한 이유는 white box test, black box test 모두를 위한 정책
  * 각 api 별로 구성하여 black box test 시 조합하여 사용 할 수 있도록 제공
  * newXXX(test case 작성 시 조합하여 사용하므로 테스트 데이터도 helper 안에 작성)
    * 사용자 정의는 lombok.builder 자유롭게 설정하고 빠르게 테스트를 하기 위함이라면  default value 사용(super.getDefaultInstance)

       | type                | default value            |
       | --------            | --------                 |
       | Long.class          | NumberUtils.LONG_ZERO    |
       | Integer.class       | NumberUtils.INTEGER_ZERO |
       | BigDecimal.class    | NumberUtils.DOUBLE_ZERO  |
       | Boolean.class       | Boolean.TRUE             |
       | String.class        | field.getName()          |
       | LocalTime.class     | LocalDate.now()          |
       | LocalDate.class     | LocalDate.now()          |
       | LocalDateTime.class | LocalDate.now()          |  
       
```java
/**
 * @since       2018.10.03
 * @author      lucas
 * @description post test helper
 **********************************************************************************************************************/
public class PostTestHelper extends TestHelper {

    @SneakyThrows
    public static void getAll(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByEntityGraph(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/entity-graph")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByQueryDsl(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/query-dsl")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByPredicate(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/predicate")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void get(Long postNo) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static ResultActions add(Add add) {
        return mock.perform(MockMvcRequestBuilders.post       ("/api/samples/posts")
                                                  .content    (ObjectHelper.getJsonForInstance(add))
                                                  .contentType(MediaType.APPLICATION_JSON))
                                                  .andExpect  (status().isOk())
                                                  .andDo      (print());
    }

    @SneakyThrows
    public static void modify(Long postNo, Modify modify) {
        mock.perform(MockMvcRequestBuilders.put        ("/api/samples/posts/{postNo}", postNo)
                                           .content    (ObjectHelper.getJsonForInstance(modify))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void remove(Long postNo) {
        mock.perform(MockMvcRequestBuilders.delete     ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void get_NotFound(Long postNo) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void modify_NotFound(Long postNo, Modify modify) {
        mock.perform(MockMvcRequestBuilders.put        ("/api/samples/posts/{postNo}", postNo)
                                           .content    (ObjectHelper.getJsonForInstance(modify))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void remove_NotFound(Long postNo) {
        mock.perform(MockMvcRequestBuilders.delete     ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }


    public static Find newFind() {
        return getDefaultInstance(PostForm.Request.Find.class);
    }

    public static Add newAdd() {
        return getDefaultInstance(PostForm.Request.Add.class);
    }

    public static Modify newModify() {
        return Modify.builder().userId ("userId")
                .title  ("title")
                .content("content").build();
    }
}
```


  ## test
  * 각 api 별로 테스트 진행
  * 예외 처리에 대한 테스트 진행
  * black box test 시 각 test helper 릅 조합하여 구성
  * 모든 api 는 해당 테스트 케이스가 만들어져야 하며 해당 테스트 케이스는 배포 시 자동진행되어 최소한의 side effect 를 감지
```java
/**
 * @since       2018.10.03
 * @author      lucas
 * @description post test
 * <pre>
 * 			      1. 정의
 * 					 api 
 * 		          2. 테스트 케이스
 * 				     - t01. 성공 - 목록 - standard
 * 				     - t02. 성공 - 목록 - entity graph
 * 				     - t03. 성공 - 목록 - query dsl(fetch join)
 * 				     - t04. 성공 - 목록 - predicate
 * 				     - t05. 성공 - 조회 - by id
 *                   - t06. 성공 - 등록
 * 				     - t07. 성공 - 수정
 * 				     - t08. 성공 - 삭제
 * 				     - t09. 실패 - 상세 - response status code 404
 * 				     - t10. 실패 - 수정 - response status code 404
 * 				     - t11. 실패 - 삭제 - response status code 404
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest	
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class PostTest {

	@Test
	public void t01_getAll() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAll(PostTestHelper.newFind());
	}
	
	@Test
	public void t02_getAllByEntityGraph() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByEntityGraph(PostTestHelper.newFind());
	}
	
	@Test
	public void t03_getAllByQueryDsl() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByQueryDsl(PostTestHelper.newFind());
	}
	
	@Test
	public void t04_getAllByPredicate() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByPredicate(PostTestHelper.newFind());
	}

	@Test 
	public void t05_get() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.get(response.getPostNo());
	}

	@Test 
	public void t06_add() {
		PostTestHelper.add(PostTestHelper.newAdd());
	} 
 
	@Test 
	public void t07_modify() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.modify(response.getPostNo(), PostTestHelper.newModify());
	} 

	@Test 
	public void t08_remove() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.remove(response.getPostNo());
	} 

	@Test 
	public void t09_get_NotFound() {
		PostTestHelper.get_NotFound(Long.MIN_VALUE);
	} 

	@Test
	public void t10_modify_NotFound() {
		PostTestHelper.modify_NotFound(Long.MIN_VALUE, PostTestHelper.newModify());
	}

	@Test 
	public void t11_remove_NotFound() {
		PostTestHelper.remove_NotFound(Long.MIN_VALUE);
	} 
}
```