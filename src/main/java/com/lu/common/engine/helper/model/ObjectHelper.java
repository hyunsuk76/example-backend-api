package com.lu.common.engine.helper.model;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lu.common.engine.helper.path.PathHelper;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description object helper
 **********************************************************************************************************************/
@Component
public class ObjectHelper {

	@Autowired 
	private ObjectHelper(ObjectMapper objectMapper) { 
		ObjectHelper.objectMapper = objectMapper; 
	}
	
	/**
	 * 파일의 JSON 문자열 반환
	 * 
	 * @param  path  JSON 경로
	 * @return       JSON 문자열
	 ******************************************************************************************************************/
	public static String getJsonForFile(String path){
		try                   { return FileUtils.readFileToString(PathHelper.getPath(path), Charset.defaultCharset()); } 
		catch (IOException e) { e.printStackTrace();                                                                   }
		
		return null;
	}
	
	/**
	 * 인스턴스의 JSON 문자열 반환
	 * 
	 * @param  o1  인스턴스
	 * @return     JSON 문자열
	 ******************************************************************************************************************/
	public static String getJsonForInstance(Object o1){
		try 							{ return objectMapper.writeValueAsString(o1); }
		catch(JsonProcessingException e){ e.printStackTrace(); 						  }
		
		return null; 
	}
	
	/**
	 * 파일의 인스턴스 반환
	 * 
	 * @param  clazz 인스턴스
	 * @param  path  JSON 경로
	 * @return       Object
	 ******************************************************************************************************************/
	public static <T> T getInstanceForFile(Class<T> clazz, String path){
		try 			  	{ return objectMapper.readValue(getJsonForFile(path), clazz); }
		catch(IOException e){ e.printStackTrace(); 					                      }
		
		return null; 
	}
	
	/**
	 * JSON 문자열의 인스턴스 반환
	 * 
	 * @param  clazz 인스턴스
	 * @param  json  인스턴스
	 * @return       Object
	 ******************************************************************************************************************/
	public static <T> T getInstanceForJson(Class<T> clazz, String json){
		try 			  	{ return objectMapper.readValue(json, clazz); }
		catch(IOException e){ e.printStackTrace(); 					      }
		
		return null; 
	}
 
	
	private static ObjectMapper objectMapper = null;
}
