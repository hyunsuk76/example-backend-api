package com.lu.sample.redis.repository;

import org.springframework.data.repository.CrudRepository;

import com.lu.sample.redis.domain.Redis;

/**   
 * @since       2018.10.15
 * @author      lucas
 * @description redis repository
 **********************************************************************************************************************/
public interface RedisRepository extends CrudRepository<Redis, String>{
	
}
