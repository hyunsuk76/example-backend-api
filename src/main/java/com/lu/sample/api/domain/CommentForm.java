package com.lu.sample.api.domain;

import lombok.Data;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description comment form
 **********************************************************************************************************************/
public class CommentForm {

	public static class Response {
		
		@Data
		public static class FindAll {

			private Long   commentNo = null;     
			private String content   = null;    
			private String createdAt = null;
			private String updatedAt = null;
		}
	}
}
