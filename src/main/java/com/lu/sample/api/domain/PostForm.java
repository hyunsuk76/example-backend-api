package com.lu.sample.api.domain;

import com.lu.common.engine.validator.form.date.DateValidator;
import com.lu.sample.api.mapper.PostMapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.mapstruct.factory.Mappers;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

/**   
 * @since       2018.10.02
 * @author      lucas
 * @description post form
 **********************************************************************************************************************/
public class PostForm {

	public static class Request {

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Find {

			@ApiModelProperty(value="사용자아이디")
			@Size(min=5)
			private String userId;

			@ApiModelProperty(value="제목")
			@Size(min=5)
			private String title;

			@ApiModelProperty(value="시작일시(형식:yyyy-MM-dd)")
			@PastOrPresent
			@DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
			private LocalDate startCreatedAt;

			@ApiModelProperty(value="종료일시(형식:yyyy-MM-dd)")
			@PastOrPresent
			@DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
			private LocalDate endCreatedAt;

			@AssertTrue
			public boolean isValidDateRange() {
				return DateValidator.isValidDateRange(startCreatedAt, endCreatedAt);
			}
		}

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Add {

			@ApiModelProperty(value="사용자아이디", required=true)
			@NotBlank
			@Size(min=5)
			private String userId;

			@ApiModelProperty(value="제목", required=true)
			@NotBlank
			@Size(min=5)
			private String title;

			@ApiModelProperty(value="내용", required=true)
			@NotBlank
			@Size(min=5)
			private String content;

			public Post toPost() {
				return mapper.toPost(this);
			}
		}

		@Getter
		@Setter
		@Builder
		@ToString
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Modify {

			@ApiModelProperty(value="사용자아이디", required=true)
			@NotBlank
			@Size(min=5)
			private String userId;

			@ApiModelProperty(value="제목", required=true)
			@NotBlank
			@Size(min=5)
			private String title;

			@ApiModelProperty(value="내용", required=true)
			@NotBlank
			@Size(min=5)
			private String content;

			public Post toPost() {
				return mapper.toPost(this);
			}
		}
	}

	public static class Response {

		@Data
		public static class FindAll {

			@ApiModelProperty(value="포스트일련번호")
			private Long   postNo;

			@ApiModelProperty(value="사용자아이디")
			private String userId;

			@ApiModelProperty(value="제목")
			private String title;

			@ApiModelProperty(value="등록일시")
			private String createdAt;

			@ApiModelProperty(value="커맨트목록")
			private List<Comment> comments;

			@Data
			public static class Comment {

				@ApiModelProperty(value="커맨트일련번호")
				private Long   commentNo;

				@ApiModelProperty(value="내용")
				private String content;
			}
		}

		@Data
		public static class FindOne {

			@ApiModelProperty(value="포스트일련번호")
			private Long   postNo;

			@ApiModelProperty(value="사용자아이디")
			private String userId;

			@ApiModelProperty(value="제목")
			private String title;

			@ApiModelProperty(value="등록일시")
			private String createdAt;

			@ApiModelProperty(value="커맨트목록")
			private List<Comment> comments;

			@Data
			public static class Comment {

				@ApiModelProperty(value="커맨트일련번호")
				private Long   commentNo;

				@ApiModelProperty(value="내용")
				private String content;
			}
		}
	}

	public static PostMapper mapper = Mappers.getMapper(PostMapper.class);
}
