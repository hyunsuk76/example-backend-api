package com.lu.sample.api.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.lu.sample.common.domain.Base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @since       2018.10.02
 * @author      lucas
 * @description post
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity(name="sample_post")
public class Post extends Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   postNo;
	private String userId;
	private String title; 
	private String content;
	private Boolean useYn;
	
	
	@OneToMany(mappedBy="post", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Comment> comments;
}
