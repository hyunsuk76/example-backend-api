package com.lu.sample.api.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.lu.sample.common.domain.Base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description comment
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity(name="sample_comment")
public class Comment extends Base {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   commentNo;
	private String content;	
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="post_no")
	private Post post;
}
