package com.lu.sample.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.lu.sample.api.domain.Post;
import com.lu.sample.api.domain.PostForm.Request.Add;
import com.lu.sample.api.domain.PostForm.Request.Modify;
import com.lu.sample.api.domain.PostForm.Response.FindAll;
import com.lu.sample.api.domain.PostForm.Response.FindOne;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post mapper  
 **********************************************************************************************************************/
@Mapper
public interface PostMapper {
    
    Post          toPost   (Add        form);
    Post          toPost   (Modify     form);
    FindAll       toFindAll(Post       entity);
    FindOne       toFindOne(Post       entity);
    List<FindAll> toFindAll(List<Post> entities);

    @Mappings({
         @Mapping(target="postNo",    ignore=true)
    	,@Mapping(target="createdAt", ignore=true)
    })
    Post modify(Post source, @MappingTarget Post target);


    PostMapper mapper = Mappers.getMapper(PostMapper.class);
}
