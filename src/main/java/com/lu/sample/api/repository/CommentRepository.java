package com.lu.sample.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lu.sample.api.domain.Comment;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description comment repository
 **********************************************************************************************************************/
public interface CommentRepository extends JpaRepository<Comment, Long>{
	
}
