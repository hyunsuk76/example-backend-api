package com.lu.sample.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lu.sample.api.domain.Post;
import com.querydsl.core.types.Predicate;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post repository querydsl
 **********************************************************************************************************************/
public interface PostRepositoryQuerydsl {

	Page<Post> findAllByQueryDsl(Predicate predicate, Pageable pageable);
}





