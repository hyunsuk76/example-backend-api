package com.lu.sample.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.lu.sample.api.domain.Post;
import com.lu.sample.api.domain.QComment;
import com.lu.sample.api.domain.QPost;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;

/**    
 * @since       2018.10.03
 * @author      lucas
 * @description post repository impl 
 **********************************************************************************************************************/
public class PostRepositoryImpl extends QuerydslRepositorySupport implements PostRepositoryQuerydsl {

	public Page<Post> findAllByQueryDsl(Predicate predicate, Pageable pageable){
		JPQLQuery<Post> query =  from    (post)
								.distinct()
			                    .leftJoin(post.comments, comment).fetchJoin()
			                    .where   (predicate)
			                    .orderBy (post.postNo.desc(), comment.commentNo.desc());
		
		return new PageImpl<>(getQuerydsl().applyPagination(pageable, query).fetch(), pageable, query.fetchCount());
	} 
	
	public PostRepositoryImpl() {
		super(Post.class);
	}
	
	private QPost    post    = QPost.post;
	private QComment comment = QComment.comment;
}




