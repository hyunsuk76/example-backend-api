package com.lu.sample.api.repository;

import java.util.Optional;

import com.lu.sample.api.domain.PostForm.Request.Find;
import com.lu.sample.api.domain.QPost;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

/**    
 * @since       2018.10.03
 * @author      lucas
 * @description post predicate 
 **********************************************************************************************************************/
public class PostPredicate {
	
	public static Predicate search(Find find) {
		
		QPost          post    = QPost.post;
		BooleanBuilder builder = new BooleanBuilder();
		
		Optional.ofNullable(find.getUserId()).ifPresent(p -> builder.and(post.userId.eq(p)));
		Optional.ofNullable(find.getTitle ()).ifPresent(p -> builder.and(post.title.eq(p)));
		
		return builder;
	}
}




