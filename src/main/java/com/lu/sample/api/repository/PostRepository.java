package com.lu.sample.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.lu.sample.api.domain.Post;
import com.querydsl.core.types.Predicate; 

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post repository 
 **********************************************************************************************************************/
@Repository
public interface PostRepository extends JpaRepository<Post, Long>, QuerydslPredicateExecutor<Post>, PostRepositoryQuerydsl {

	Post findOneByPostNo(Long postNo);
	
	@EntityGraph(attributePaths="comments", type=EntityGraphType.FETCH)
	Page<Post> findAll(Pageable pageable);
	
	@EntityGraph(attributePaths="comments", type=EntityGraphType.FETCH)
	List<Post> findAll(Predicate predicate);
}
