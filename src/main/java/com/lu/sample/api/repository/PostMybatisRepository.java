package com.lu.sample.api.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.lu.sample.api.domain.Post;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post mybatis repostiry
 **********************************************************************************************************************/
@Mapper
public interface PostMybatisRepository {
	
	List<Post> findAll();
}
