package com.lu.sample.api.service;

import static com.lu.sample.api.mapper.PostMapper.mapper;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lu.sample.api.domain.Post;
import com.lu.sample.api.repository.PostRepository;
import com.querydsl.core.types.Predicate;

import lombok.RequiredArgsConstructor;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class PostService {

	@Transactional(readOnly = true)
	public Page<Post> getAll(Pageable pageable) {
		return postRepository.findAll(pageable);
	}
	
	@Transactional(readOnly = true)
	public Page<Post> getAllByEntityGraph(Pageable pageable) {		
		return postRepository.findAll(pageable);
	}
	
	@Transactional(readOnly = true)
	public Page<Post> getAllByQueryDsl(Predicate predicate, Pageable pageable) {
		return postRepository.findAllByQueryDsl(predicate, pageable);
	}
	
	@Transactional(readOnly = true)
	public List<Post> getAllByPredicate(Predicate predicate) {		
		return postRepository.findAll(predicate);
	}
	
	@Transactional(readOnly = true)	
	public Post get(Long postNo) {
		return postRepository.getOne(postNo);
	}

	@Transactional(readOnly=true)
	public Post getOneByPostNo(Long postNo) {
		return postRepository.findOneByPostNo(postNo);
	}
	
	public Post add(Post post) {
		return postRepository.save(post); 
	}
	
	public Post modify(Long postNo, Post post) {
		return mapper.modify(post, postRepository.getOne(postNo));
	}
	
	public void remove(Long postNo) {
		postRepository.delete(postRepository.getOne(postNo));
	}


	private final PostRepository postRepository;
}

