package com.lu.sample.api.service;

import com.lu.sample.api.domain.Comment;
import com.lu.sample.api.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**   
 * @since       2018.10.19
 * @author      lucas
 * @description comment service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class CommentService {

	@Transactional(readOnly = true)
	public Page<Comment> getAll(Pageable pageable) {
		return commentRepository.findAll(pageable);
	}

	@Transactional(readOnly = true)
	public Comment getOne(Long postNo) {
		return commentRepository.getOne(postNo);
	}


	private final CommentRepository commentRepository;
}

