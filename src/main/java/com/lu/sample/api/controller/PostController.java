package com.lu.sample.api.controller;

import com.lu.common.engine.exception.NotAcceptableException;
import com.lu.sample.api.domain.PostForm.Request.Add;
import com.lu.sample.api.domain.PostForm.Request.Find;
import com.lu.sample.api.domain.PostForm.Request.Modify;
import com.lu.sample.api.domain.PostForm.Response.FindAll;
import com.lu.sample.api.domain.PostForm.Response.FindOne;
import com.lu.sample.api.repository.PostPredicate;
import com.lu.sample.api.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.lu.sample.api.mapper.PostMapper.mapper;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post controller
 **********************************************************************************************************************/
@Api(description="샘플")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class PostController {

	@ApiOperation("포스트 페이징 목록 - 일반적인")
	@GetMapping("/samples/posts")
	public Page<FindAll> getAll(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAll(pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 페이징 목록 - Entity Graph")
	@GetMapping("/samples/posts/entity-graph")
	public Page<FindAll> getAllByEntityGraph(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAllByEntityGraph(pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 페이징 목록 - Querydsl")
	@GetMapping("/samples/posts/query-dsl")
	public Page<FindAll> getAllByQueryDsl(@Valid Find find, @PageableDefault Pageable pageable){
		return postService.getAllByQueryDsl(PostPredicate.search(find), pageable).map(mapper::toFindAll);
	}

	@ApiOperation("포스트 목록 - Predicate")
	@GetMapping("/samples/posts/predicate")
	public List<FindAll> getAllByPredicate(@Valid Find find){
		return mapper.toFindAll(postService.getAllByPredicate(PostPredicate.search(find)));
	}

	@ApiOperation("포스트 상세 조회")
	@GetMapping("/samples/posts/{postNo}")
	public FindOne get(@PathVariable Long postNo){
		return mapper.toFindOne(postService.get(postNo));
	}

	@ApiOperation("포스트 등록")
	@PostMapping("/samples/posts")
	public FindOne add(@Valid @RequestBody Add add){
		return mapper.toFindOne(postService.add(add.toPost()));
	}

	@ApiOperation("포스트 수정")
	@PutMapping("/samples/posts/{postNo}")
	public FindOne modify(@PathVariable Long postNo, @Valid @RequestBody Modify modify){
		return mapper.toFindOne(postService.modify(postNo, modify.toPost()));
	}

	@ApiOperation("포스트 삭제")
	@DeleteMapping("/samples/posts/{postNo}")
	public void remove(@PathVariable Long postNo){
		postService.remove(postNo);
	}

	@ApiOperation("포스트 존재 여부")
	@RequestMapping(method= RequestMethod.HEAD, value="/samples/posts/{postNo}")
	public void isExists(@PathVariable Long postNo){
		Optional.ofNullable(postService.getOneByPostNo(postNo)).orElseThrow(NotAcceptableException::new);
	}


	private final PostService postService; 
}




