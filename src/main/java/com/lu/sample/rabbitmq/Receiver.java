package com.lu.sample.rabbitmq;


import java.util.concurrent.CountDownLatch;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Receiver {

	public void receiveMessage(String message) {
        log.debug("Received <" + message + ">");
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    
    private CountDownLatch latch = new CountDownLatch(1);
}