package com.lu.common.engine.helper.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lu.sample.api.domain.PostForm;

/**   
 * @since       2018.10.10
 * @author      lucas
 * @description test helper test
 * <pre>
 * 				  1. 정의
 *                   빠르게 테스트하기 위해 테스트 데이터 정의 시 타입별로 기본값을 설정하여 반환하도록 구성하였고
 *                   기본값은 아래와 같다.
 *                   - @org.springframework.data.annotation.Id 제외
 *                   - Long.class          : NumberUtils.LONG_ZERO   
 *                   - Integer.class       : NumberUtils.INTEGER_ZERO
 *                   - BigDecimal.class    : NumberUtils.DOUBLE_ZERO 
 *                   - Boolean.class       : Boolean.TRUE            
 *                   - String.class        : field.getName()         
 *                   - LocalTime.class     : LocalDate.now()         
 *                   - LocalDate.class     : LocalDate.now()         
 *                   - LocalDateTime.class : LocalDate.now()         
 * 				  2. 테스트 케이스
 *                   - t01. 성공 - 기본값으로 세팅되어 반환되었는지 확인
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest	
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHelperTest extends TestHelper {

	@Test
	public void t01_getDefaultInstance() {
		
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Add.class   )).isNotNull();
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Find.class  )).isNotNull();
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Modify.class)).isNotNull();
		
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Add.class   ).getUserId ()).isEqualTo("userId");
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Find.class  ).getTitle  ()).isEqualTo("title");
		assertThat(TestHelper.getDefaultInstance(PostForm.Request.Modify.class).getContent()).isEqualTo("content");
	}
}
