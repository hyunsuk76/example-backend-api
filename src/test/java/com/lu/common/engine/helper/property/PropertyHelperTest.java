package com.lu.common.engine.helper.property;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lu.common.engine.helper.test.TestHelper;

/**   
 * @since       2018.10.10
 * @author      lucas
 * @description property helper test
 * <pre>
 * 			      1. 정의
 * 					 application.yml에 정의 된 property. 이하의 정보를 조회 테스트
 * 		          2. 테스트 케이스
 *                   - t01. 성공 - 조회
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest	
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PropertyHelperTest extends TestHelper {

	@Test
	public void t01_find() {
		
		String title   = PropertyHelper.getProperty().getSwagger().getInfo().getTitle();
		String version = PropertyHelper.getProperty().getSwagger().getInfo().getVersion();
		
		assertNotNull(title  );
		assertNotNull(version);
	}
}
