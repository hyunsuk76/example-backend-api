package com.lu.common.engine.helper.message;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lu.common.engine.exception.common.ExceptionCode;
import com.lu.common.engine.helper.test.TestHelper;

/**   
 * @since       2018.10.10
 * @author      lucas
 * @description message helper test
 * <pre>
 * 			      1. 정의
 * 					 message_xxx.properties 에 정의 된 메세지 조회 테스트
 * 		          2. 테스트 케이스
 *                   - t01. 성공 - 조회 - by message code id
 *                   - t02. 성공 - 조회 - by exception code id
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest	
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MessageHelperTest extends TestHelper {

	@Test
	public void t01_get() {
		
		assertNotNull(MessageHelper.getMessage(MessageCode.M00010001));
	}
	
	@Test
	public void t02_get() {
		
		assertNotNull(MessageHelper.getMessage(ExceptionCode.E00010001));
	}
}
