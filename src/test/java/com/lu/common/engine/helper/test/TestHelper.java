package com.lu.common.engine.helper.test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.lu.common.engine.helper.model.ObjectHelper;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description test helper
 **********************************************************************************************************************/
@NoArgsConstructor
@Component 
public class TestHelper {

	@Autowired
	public TestHelper(WebApplicationContext context){
		mock = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@SneakyThrows
	public static <T> T getInstance(ResultActions resultActions, Class<T> clazz) {
		return ObjectHelper.getInstanceForJson(clazz, resultActions.andReturn().getResponse().getContentAsString());
	}

	@SneakyThrows
	public static <T> T getDefaultInstance(Class<T> clazz) {
		
		T t = clazz.newInstance();
		for(Field field : t.getClass().getDeclaredFields()) {
			
			if( null != field.getAnnotation(Id.class) ) {
				continue;
			}
			
			field.setAccessible(true);
			     if(Long.class          == field.getType()) { field.set(t, NumberUtils.LONG_ZERO);    }
			else if(Integer.class       == field.getType()) { field.set(t, NumberUtils.INTEGER_ZERO); }
			else if(BigDecimal.class    == field.getType()) { field.set(t, NumberUtils.DOUBLE_ZERO);  }
			else if(Boolean.class       == field.getType()) { field.set(t, Boolean.TRUE);             }
			else if(String.class        == field.getType()) { field.set(t, field.getName());          }
			else if(LocalTime.class     == field.getType()) { field.set(t, LocalDate.now());          }
			else if(LocalDate.class     == field.getType()) { field.set(t, LocalDate.now());          }
			else if(LocalDateTime.class == field.getType()) { field.set(t, LocalDate.now());          }
		}
		
		return t; 
	}


	protected static MockMvc mock = null;
}
