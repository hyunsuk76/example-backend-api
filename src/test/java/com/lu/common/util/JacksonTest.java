package com.lu.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.lu.common.engine.helper.test.TestHelper;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**   
 * @since       2018.10.26
 * @author      lucas
 * @description jackson test
 * <pre>
 * 			      1. 정의
 * 					 object <-> yaml
 * 		          2. 테스트 케이스
 *                   - t01. 성공 - object -> yaml
 *                   - t02. 성공 - yaml   -> object
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Slf4j
public class JacksonTest extends TestHelper {

	private User data = null;


	@Before
	public void init() {

		data = User.builder().userId ("user1")
				             .address("seoul")
				             .cards  (
				             		Lists.newArrayList(
				             				 Card.builder().cardNo(1L).cardName("bc").bcCard(BcCard.builder().bcCardNo(1L).number("123456").build()).build()
				             				,Card.builder().cardNo(2L).cardName("kb").bcCard(BcCard.builder().bcCardNo(2L).number("345657").build()).build()
				             				,Card.builder().cardNo(3L).cardName("nb").bcCard(BcCard.builder().bcCardNo(3L).number("797789").build()).build()
				             		)
				             )
				             .build();
	}

	@Test
	@SneakyThrows
	public void t01_toYaml() {

		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		String       yaml   = mapper.writeValueAsString(data);

		log.debug(yaml);

		assertThat(yaml).isNotNull();
		assertThat(yaml).isNotBlank();
	}

	@Test
	@SneakyThrows
	public void t02_toObject() {

		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		String       yaml   = mapper.writeValueAsString(data);
		User         user   = mapper.readValue(yaml, User.class);

		assertThat(yaml).isNotNull();
		assertThat(yaml).isNotBlank();
		assertThat(user).isNotNull();
		assertThat(user.getUserId()).isEqualTo("user1");
		assertThat(user.getCards()).isNotNull();
		assertThat(user.getCards().get(0).getCardName()).isEqualTo("bc");
	}

	@Test
	@SneakyThrows
	public void t03_toJson() {

		assertThat(true).isTrue();
	}


	@Data
	@Builder
	public static class User {

		private String     userId;
		private String     address;
		private List<Card> cards;
	}

	@Data
	@Builder
	public static class Card {

		private Long   cardNo;
		private String cardName;
		private BcCard bcCard;
	}

	@Data
	@Builder
	public static class BcCard {

		private Long   bcCardNo;
		private String number;
	}
}

