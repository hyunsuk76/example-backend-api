package com.lu.sample.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lu.sample.api.domain.Post;
import com.lu.sample.api.repository.PostMybatisRepository;

/**   
 * @since       2018.10.03
 * @author      lucas
 * @description post repository test
 * <pre> 
 * 			      1. 정의
 * 					 sql mapper
 * 		          2. 테스트 케이스
 * 				     - t01. 성공 - 목록
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest
@RunWith(SpringRunner.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PostRepositoryTest {
	
	@Test
	public void t01_findAll() {
		
		List<Post> posts = postMybatisRepository.findAll();
		assertThat(posts).isNotNull();
	}
	

	@Autowired private PostMybatisRepository postMybatisRepository;
}



