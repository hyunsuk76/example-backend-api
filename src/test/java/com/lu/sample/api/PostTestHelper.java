package com.lu.sample.api;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import lombok.SneakyThrows;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.lu.common.engine.helper.model.ObjectHelper;
import com.lu.common.engine.helper.test.TestHelper;
import com.lu.sample.api.domain.PostForm;
import com.lu.sample.api.domain.PostForm.Request.Add;
import com.lu.sample.api.domain.PostForm.Request.Find;
import com.lu.sample.api.domain.PostForm.Request.Modify;

/**
 * @since       2018.10.03
 * @author      lucas
 * @description post test helper
 **********************************************************************************************************************/
public class PostTestHelper extends TestHelper {

    @SneakyThrows
    public static void getAll(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByEntityGraph(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/entity-graph")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByQueryDsl(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/query-dsl")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void getAllByPredicate(Find find) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/predicate")
                                           .content    (ObjectHelper.getJsonForInstance(find))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void get(Long postNo) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static ResultActions add(Add add) {
        return mock.perform(MockMvcRequestBuilders.post       ("/api/samples/posts")
                                                  .content    (ObjectHelper.getJsonForInstance(add))
                                                  .contentType(MediaType.APPLICATION_JSON))
                                                  .andExpect  (status().isOk())
                                                  .andDo      (print());
    }

    @SneakyThrows
    public static void modify(Long postNo, Modify modify) {
        mock.perform(MockMvcRequestBuilders.put        ("/api/samples/posts/{postNo}", postNo)
                                           .content    (ObjectHelper.getJsonForInstance(modify))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void remove(Long postNo) {
        mock.perform(MockMvcRequestBuilders.delete     ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isOk())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void get_NotFound(Long postNo) {
        mock.perform(MockMvcRequestBuilders.get        ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void modify_NotFound(Long postNo, Modify modify) {
        mock.perform(MockMvcRequestBuilders.put        ("/api/samples/posts/{postNo}", postNo)
                                           .content    (ObjectHelper.getJsonForInstance(modify))
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }

    @SneakyThrows
    public static void remove_NotFound(Long postNo) {
        mock.perform(MockMvcRequestBuilders.delete     ("/api/samples/posts/{postNo}", postNo)
                                           .contentType(MediaType.APPLICATION_JSON))
                                           .andExpect  (status().isNotFound())
                                           .andDo      (print());
    }


    public static Find newFind() {
        return getDefaultInstance(PostForm.Request.Find.class);
    }

    public static Add newAdd() {
        return getDefaultInstance(PostForm.Request.Add.class);
    }

    public static Modify newModify() {
        return Modify.builder().userId ("userId")
                .title  ("title")
                .content("content").build();
    }
}
