package com.lu.sample.api;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.lu.common.engine.helper.test.TestHelper;
import com.lu.sample.api.domain.PostForm.Response.FindOne;

/**
 * @since       2018.10.03
 * @author      lucas
 * @description post test
 * <pre>
 * 			      1. 정의
 * 					 api 
 * 		          2. 테스트 케이스
 * 				     - t01. 성공 - 목록 - standard
 * 				     - t02. 성공 - 목록 - entity graph
 * 				     - t03. 성공 - 목록 - query dsl(fetch join)
 * 				     - t04. 성공 - 목록 - predicate
 * 				     - t05. 성공 - 조회 - by id
 *                   - t06. 성공 - 등록
 * 				     - t07. 성공 - 수정
 * 				     - t08. 성공 - 삭제
 * 				     - t09. 실패 - 상세 - response status code 404
 * 				     - t10. 실패 - 수정 - response status code 404
 * 				     - t11. 실패 - 삭제 - response status code 404
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest	
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
public class PostTest {

	@Test
	public void t01_getAll() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAll(PostTestHelper.newFind());
	}
	
	@Test
	public void t02_getAllByEntityGraph() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByEntityGraph(PostTestHelper.newFind());
	}
	
	@Test
	public void t03_getAllByQueryDsl() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByQueryDsl(PostTestHelper.newFind());
	}
	
	@Test
	public void t04_getAllByPredicate() {
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.add(PostTestHelper.newAdd());
		PostTestHelper.getAllByPredicate(PostTestHelper.newFind());
	}

	@Test 
	public void t05_get() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.get(response.getPostNo());
	}

	@Test 
	public void t06_add() {
		PostTestHelper.add(PostTestHelper.newAdd());
	} 
 
	@Test 
	public void t07_modify() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.modify(response.getPostNo(), PostTestHelper.newModify());
	} 

	@Test 
	public void t08_remove() {
		FindOne response = TestHelper.getInstance(PostTestHelper.add(PostTestHelper.newAdd()), FindOne.class);
		PostTestHelper.remove(response.getPostNo());
	} 

	@Test 
	public void t09_get_NotFound() {
		PostTestHelper.get_NotFound(Long.MIN_VALUE);
	} 

	@Test
	public void t10_modify_NotFound() {
		PostTestHelper.modify_NotFound(Long.MIN_VALUE, PostTestHelper.newModify());
	}

	@Test 
	public void t11_remove_NotFound() {
		PostTestHelper.remove_NotFound(Long.MIN_VALUE);
	} 
}



