package com.lu.sample.redis;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.lu.common.engine.helper.test.TestHelper;
import com.lu.sample.redis.domain.Redis;
import com.lu.sample.redis.service.RedisService;

/**
 * @since       2018.10.03
 * @author      lucas
 * @description redis test
 * <pre>
 * 			      1. 정의
 * 					 redis
 * 		          2. 테스트 케이스
 *                   - t01. 성공 - 등록
 * 				     - t02. 성공 - 조회 - by key
 * 				     - t03. 성공 - 목록
 * 				     - t04. 성공 - 목록 - by keys
 * </pre>
 **********************************************************************************************************************/
@SpringBootTest
@RunWith(SpringRunner.class) 
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class RedisTest {

	@Test
	public void t01_add() {
		
		Redis meeting = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		assertThat(meeting).isNotNull();
	}
	
	@Test
	public void t02_get() {
		
		Redis addMeeting = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		assertThat(addMeeting).isNotNull();
		
		Optional<Redis> meeting = redisService.get(addMeeting.getId());
		assertThat(meeting.isPresent()).isTrue();
		assertThat(meeting.get()).isNotNull();
	}
	
	@Test
	public void t03_getAll() {
		
		Redis addMeeting1 = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		Redis addMeeting2 = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		assertThat(addMeeting1).isNotNull();
		assertThat(addMeeting2).isNotNull();
		
		Iterable<Redis> meetings = redisService.getAll(Lists.newArrayList(addMeeting1.getId(), addMeeting2.getId()));
		assertThat(meetings).isNotEmpty();
	}
	
	@Test
	public void t04_getAll() {
		
		Redis addMeeting1 = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		Redis addMeeting2 = redisService.add(TestHelper.getDefaultInstance(Redis.class));
		assertThat(addMeeting1).isNotNull();
		assertThat(addMeeting2).isNotNull();
		
		Iterable<Redis> meetings = redisService.getAll();
		assertThat(meetings).isNotEmpty();
	}
	

	@Autowired private RedisService redisService;
}
